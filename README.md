# Namecheap dynamic dns script

This script dynamically updates your domains ip with an http request.


## Using the script

To begin with clone the repository. Make sure that the script has execution priviledges by running this command inside the repository folder:

```
# chmod u+x dynamicdns.sh
```

Then use your favourite text editor to open the script. Inside we need to add a bit more information.

We need to tell the script the host to update, it has @ as default but you can change it.

In the next line we need to tell it the domain to update.

And lastly we need to add the password, you can get this by going to your account -> Domain List -> Manage -> Advanced DNS -> Dynamic DNS. If it is not enabled, enable it to check the password.

It should look like something like this:

```
host=@
domain=exampledomain.com
password=lf2viar2alsajyida7zb5pawbjmacwms
```

## Running the script

The script can be manually run from the terminal but it might be more convenient to run it automatically periodically. The script will only send an http request when it sees that the ip doesn't match.

To do this we can use cron. How often the script is run is up to the user but doing it once a day is more than enough.

To do this we can use cron. We run the command

```
crontab -e
```

and then we write this line:

```
* 23 * * * /path/to/the/script/dynamicdns.sh
```

This will run the script every day at 23:00.
