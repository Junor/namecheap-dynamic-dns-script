#!/bin/bash
host=@
domain=
password=

domain_ip=$(drill -Q $domain)
ip=$(curl -s ifconfig.me/ip)

if [[ $domain_ip != $ip ]]; then
  curl "https://dynamicdns.park-your-domain.com/update?host=$host&domain=$domain&password=$password&ip=$ip"
fi

